<?php
// process.php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('includes/functions.php');

$errors         = array();      // array to hold validation errors
$data           = array();      // array to pass back data

// validate the variables ======================================================
    // if any of these variables don't exist, add an error to our $errors array

    if (empty($_POST['formday']))
        $errors['day'] = 'Day of Birth is required.';

    if (empty($_POST['formyear']))
        $errors['year'] = 'Year of Birth is required.';

    if (empty($_POST['formmonth']))
        $errors['month'] = 'Month of Birth is required.';
    $day = intval($_POST['formday']);
    $month = intval($_POST['formmonth']);
    $year = intval($_POST['formyear']);
    // console_log($day);
//     console.log($_POST['formmonth']);
// console.log($_POST['formyear']);

// return a response ===========================================================

    // if there are any errors in our errors array, return a success boolean of false
    if ( ! empty($errors)) {

        // if there are items in our errors array, return those errors
        $data['success'] = false;
        $data['errors']  = $errors;
       
    } else {

        // if there are no errors process our form, then return a message

        // DO ALL YOUR FORM PROCESSING HERE
        // THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)

        // show a message of success and provide a true success variable
        $data['success'] = true;
        $data['message'] = 'Success!';
      
        $number = getLifePathNumber($day,$month,$year);

        $data['lifenumber'] = $number; 
        
        $lpninfo  = getDbInfo($number);
        $lpntable = getTable($number,$year); //calculate the table grid we will display

        
        $data['table'] = $lpntable;
        $data['lpn']   =  $lpninfo['lpn'];
        $data['info']  =  nl2br($lpninfo['info']);
        // console_log($number);
        $data['lifenumber'] = $number;        
        
          
    }

    // return all our data to an AJAX call
    echo json_encode($data);
