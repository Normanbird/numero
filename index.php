<?php
require('includes/functions.php');
?>
<title>Numerology</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<link rel="stylesheet" href="includes/style.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="magic.js"></script> <!-- load our javascript file -->
</head>
</head>
<body>
<?php
$num = 2;
$age = 0;
$year = 1965;
$lifepathnumber = null;
$numberinfo = "unknown";
$earliest_year = 1885;
// Set your latest year you want in the range, in this case we use PHP to just set it to the current year.
$latest_year = intval(date('Y'));
?>


<div class="container">

    <h1>Numerology.</h1>
    <div class="opening">
         The Life Path Number is the sum of the birth date. This number represents who you are at birth and the native traits that you will carry with you through life. The most important number that will be discussed here is your <strong>Life Path number.</strong> The Life Path describes the nature of this journey through life. The Life Path number is established from the date of birth. First, add the Month and the Day together, then reduce that number to a single digit. Then reduce your birth year. Now finally, add the first number to the second number and reduce it as well to a single digit. THIS is your Life Path Number. We have made it easy for you; just enter your birthday and we will do all the work. 
    </div>

   

       
    <form class="form1 " action="process.php" method="post">
        
        <div class="form-row">
            <div class="col">
                
                <label for="formmonth">Date Of Birth Month</label>
                
                <select id="formmonth"  name="formmonth"  class="form-control " />
                    <option value=1>January</option>
                    <option value=2>February</option>
                    <option value=3>March</option>
                    <option value=4>April</option>
                    <option value=5>May</option>
                    <option value=6>June</option>
                    <option value=7>July</option>
                    <option value=8>August</option>
                    <option value=9>September</option>
                    <option value=10>October</option>
                    <option value=11>November</option>
                    <option value=12>December</option>
                </select>
                
            </div>
            
            
            <div class="col">
                
                <label for="formday">Day</label>
                <select id="formday" name="formday"  class="form-control  " />
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                    <option>11</option>
                    <option>12</option>
                    <option>13</option>
                    <option>14</option>
                    <option>15</option>
                    <option>16</option>
                    <option>17</option>
                    <option>18</option>
                    <option>19</option>
                    <option>20</option>
                    <option>21</option>
                    <option>22</option>
                    <option>23</option>
                    <option>24</option>
                    <option>25</option>
                    <option>26</option>
                    <option>27</option>
                    <option>28</option>
                    <option>29</option>
                    <option>30</option>
                    <option>31</option>
                </select>
                
            </div>
            <div class="col">
                
                <label for="formday">Year</label>
                <?php

               print '<select id="formyear" name="formyear"  class="form-control" >';
                    // Sets the top option to be the current year. (IE. the option that is chosen by default).
                    $currently_selected = date('Y');
                    // Loops over each int[year] from current year, back to the $earliest_year [1950]
                    foreach ( range( $latest_year, $earliest_year ) as $i ) {
                    // Prints the option with the next year in range.
                    print '<option value='.$i.'>'.$i.'</option>';
                    }
                print '</select>';
                ?>
                
            </div>
        </div>
        
        <button type="submit" class="btn btn-primary">Submit </button>
    </form>


    <!-- div that will be updated by ajax -->
    <div id="lpninfodiv" class="col-lg-12 lpninfodiv">
        <h1>Your lifepath number is:<span id="lpntitle" class="lpntitle"></span></h1>
        
        <div id="lpninfo" class="lpninfo">
            
        </div>
    </div>
    <!-- end of div that will be updated by ajax, with lifepath info -->
    

<div id="lpntablediv">    


    <h3>Life Path Table</h3>

      <div class="opening">
          <p>Below is an informative Life Path table which tracks the Life path years 1-9, as they pertain to more advanced numerology. It is designed to help you visualize the various data points that revolve around your Life Path number every 9 years.</p> 

          <p>For each year of your life, you can see what year of the 9 year cycle you are in, and exactly which 9th year cycle you are in; be it the 3rd 9th year or 4th etc.</p> 

          <p>For instance, it is beneficial to know if you are in the building years of 1-3, where you have to create your next cycle, the middle 3-6 years where you are starting to see the cycle come together, or the final 7-9 years of your 9 year cycle which show the most reward and satisfaction from all your hard work.</p> 

          <p>In the 9th year is when our cycles "wrap up" and we will often see a "change in the theme" of our lives. So for the trained eye, the table below can be a blueprint of change, as it pertains to your life. It can be very accurate.</p> 
    </div>
   <table id="lpinfotable" class="table table-striped">
        <thead>
            <tr>
                <th scope="col">1</th>
                <th scope="col">2</th>
                <th scope="col">3</th>
                <th scope="col">4</th>
                <th scope="col">5</th>
                <th scope="col">6</th>
                <th scope="col">7</th>
                <th scope="col">8</th>
                <th scope="col">9</th>
            </tr>
        </thead><tbody id="lpnbody"> 
<!-- here will go the info calculated via ajax push -->
    

        </tbody></table>
        

    </div> <!-- lpntablediv -->
</div> <!-- container -->
</body>
</html>