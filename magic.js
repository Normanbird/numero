// magic.js
$(document).ready(function() {

    $("#lpninfodiv").hide();

    // process the form
    $('form').submit(function(event) {

        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'formday'           : $('select[name=formday]').val(),
            'formyear'          : $('select[name=formyear]').val(),
            'formmonth'         : $('select[name=formmonth]').val()
        };

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'process.php', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'JSON', // what type of data do we expect back from the server
            encode          : true
        })
            // using the done promise callback
            .done(function(data) {
                // log data to the console so we can see
                console.log(data); 

                $("#lpninfo").html(data.info);
                $("#lpntitle").html(data.lpn);
                $("#lpnbody").html(data.table);
                $("#lpninfodiv").show(618);

                // here we will handle errors and validation messages
            });


            // using the fail promise callback
            // .fail(function(data) {

            //     // show any errors
            //     // best to remove for production
            //     console.log(data);
            // });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

});